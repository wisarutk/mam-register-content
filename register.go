package register

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/kpango/glg"
	"github.com/vansante/go-ffprobe"
)

type Data struct {
	Filename  string `json:"fileName"`
	FileSize  string `json:"file_size"`
	WatchPath string `json:"watch_path"`
	Status    string `json:"status"`
	Info      FFout  `json:"info"`
}

type FFout struct {
	Streams []ffprobe.Stream `json:"streams"`
	Format  ffprobe.Format   `json:"format"`
}

type RespBodyRegisMAM struct {
	StatusCode  int    `json:"status_code"`
	Description string `json:"description"`
	Info        Info   `json:"info"`
}

type Info struct {
	AssetID    string      `json:"asset_id"`
	NextPath   []string    `json:"next_path"`
	Type       string      `json:"type"`
	ReFilename string      `json:"re_filename"`
	Status     string      `json:"status"`
	Exec       interface{} `json:"exec"`
}

type InputTransferWF struct {
	ID                 string   `json:"id"`
	SrcNodeControl     string   `json:"src_node_control"`
	SrcNodeControlUser string   `json:"src_node_control_user"`
	SrcNodeControlPW   string   `json:"src_node_control_pw"`
	RemoteAddr         string   `json:"remote_addr"`
	RemoteUser         string   `json:"remote_user"`
	RemotePW           string   `json:"remote_pw"`
	TargetPath         []string `json:"target_path"`
	SrcAbPath          []string `json:"src_ab_path"`
	SrcBasePath        string   `json:"src_base_path"`
}

func Register(urlMAM, token string, data interface{}) (RespBodyRegisMAM, error) {
	body, err := json.Marshal(data)
	if err != nil {
		return RespBodyRegisMAM{}, err
	}
	respRegis := RespBodyRegisMAM{}

	bodyRegister := Data{}
	err = json.Unmarshal(body, &bodyRegister)
	if err != nil {
		return RespBodyRegisMAM{}, err
	}

	// glg.Info("before unmarshal:")
	// jsonData, err := json.MarshalIndent(body, "", "	")
	// if err != nil {
	// 	return RespBodyRegisMAM{}, err
	// }
	// glg.Info(string(jsonData))

	// glg.Info("after unmarshal:")
	// jsonData, err = json.MarshalIndent(bodyRegister, "", "	")
	// if err != nil {
	// 	return RespBodyRegisMAM{}, err
	// }
	// glg.Info(string(jsonData))

	body, err = json.Marshal(bodyRegister)
	if err != nil {
		return RespBodyRegisMAM{}, err
	}

	timeout := 10 * time.Second
	client := &http.Client{
		Timeout: timeout,
	}

	req, err := http.NewRequest("POST", urlMAM+"/asset/flow", bytes.NewBuffer(body))
	if err != nil {
		// handle error
		return RespBodyRegisMAM{}, err
	}

	glg.Info("Token:", token)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Token", token)

	resp, err := client.Do(req)
	if err != nil {
		// handle error
		return RespBodyRegisMAM{}, err
	}
	defer resp.Body.Close()

	// resp, err := http.Post(urlMAM+"/asset/flow", "application/json", bytes.NewBuffer(body))
	// if err != nil {
	// 	// handle error
	// 	return RespBodyRegisMAM{}, err
	// }

	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return RespBodyRegisMAM{}, err
	}
	glg.Info("response from start workflow", resp.Status, string(body))

	if resp.Status != "200 OK" {
		return RespBodyRegisMAM{}, errors.New("call mam api error : " + resp.Status + " : " + string(body))
	}

	json.Unmarshal(body, &respRegis)

	//temp := RespBodyRegisMAM.Info{AssetID: "y24N9L7LQQwq", NextPath: string{"/live_to_vod/transcode_file/mam/tmp_in/", "/Archive_G4002/source_vod/clip_raw_file/", "/clip/", "/Archive_V7k/sent_clip_prd_mtg_tyn/"}, Type: "clip", ReFilename: "y24N9L7LQQwq", Status: "receive", Exec: nil}
	// return RespBodyRegisMAM{
	// 	StatusCode:  200,
	// 	Description: "Success",
	// 	Info: Info{
	// 		AssetID:    "Test",
	// 		NextPath:   []string{"/sas_content/staging_shelf_Philippines/", "/sas_content/staging_shelf_Indonesia/"},
	// 		Type:       "clip",
	// 		ReFilename: "Test",
	// 		Status:     "receive",
	// 		Exec:       nil,
	// 	},
	// }, nil

	return respRegis, nil
}
